# Functions 	
	echo_usage() {
		echo "Usage: backup [-h db_hostname_address] [ -u database_username] [ -p database_password ]"
	}

# 0. Check arguments

	while getopts u:p: flag
		do
		    case "${flag}" in
		    	h) hostname=${OPTARG};;
		        u) username=${OPTARG};;
		        p) password=${OPTARG};;
		    esac
	done

	if [[ $# -eq 0 || -z $username || -z $password ]]; then
		echo_usage
		exit 1
	fi

	timestamp=$(date "+%Y-%m-%d-%Z-%H-%M-%S")

	echo "Hostname: $hostname";
	echo "Username: $username";
	echo "Password: $password";
	echo "Timestamp: $timestamp"

# 1. Database dump and zip

	# Test database connection

# 2. Copy wp-content
